# MySQL备份还原类库

提供Mysql导入导出等基础功能，需要`php>=5.4`,依赖`pdo_mysql`扩展，后续也许会完善增强

已加入composer豪华午餐

安装

```$xslt
composer require maowenke/mysql dev-master
```

使用

```$xslt
require '/vendor/autoload.php';
$database = [
	'username' => 'root',
	'password' => 'root',
	'host' => '127.0.0.1:3306',
	'database' => 'table',
];
$back_tool = new jayfun\mysqlTool\Backup($database);
$filepath = $back_tool->setWithData(true)->setFilename('backup')->dump();
$bool = $back_tool->setTables(['ea_system_ces'])->setOutputPath(public_path())->setWithData(true)->dump(['where id > 10']);

/* filepath 是一个数组，第一个元素是相对路径，第二个元素是绝对路径 */

$restore = new \jayfun\mysqlTool\Restore($database);
$restore->restore($filepath[0]);
```